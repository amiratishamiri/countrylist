import React from 'react'
import Styled from 'styled-components'


const StyledCard = Styled.div`
    box-shadow:0px 5px 18px -3px rgba(7,7,7,0.32);
    display:flex;
    flex-direction:column;
    background-color:#FAFAFA;
    padding:2px;
    margin:25px 4px 4px 15px;
    border-radius:10px;
    & > img{
        width:100%;
        border-radius:10px 10px 0px 0px;
        object-fit:cover;
    }
    & > p{
        padding:10px 10px;
    }
`

const ImageCard = ({ title, image, className, ...props }) => {

    return (
        <StyledCard className={className} {...props}>
            <img src={image} />
            <p>
                {title}
            </p>

        </StyledCard>
    )

}
export default ImageCard;