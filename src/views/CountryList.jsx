import React, { useEffect, useState } from 'react'
import Styled from 'styled-components'
import axios from 'axios';
import ImageCard from '../components/ImageCard';


const StyledList = Styled.div`
 
    display: flex;
    justify-content: space-around;
    
    & li{        
        list-style:none;
        animation:fadeInUp;
        animation-duration:1s;    
        & .__country-item{
        width:400px;
        }
    }
`

const CountryList = ({ className, ...props }) => {
    const [data, setData] = useState([]);
    useEffect(() => {
        axios.get(
            'https://restcountries.com/v2/all?fields=name,flags'
        ).then(
            res => { setData(res.data); console.log(res.data) }
        ).catch(e =>
            console.error(e)
        )

    }, [])
    return (
        <StyledList className={className} {...props}>
            <ul>
                {
                    data.map(item => <li>
                        <ImageCard className='__country-item' title={item.name} image={item?.flags?.png} />
                    </li>)
                }
            </ul>

        </StyledList>
    )

}
export default CountryList;