import './App.css';
import CountryList from './views/CountryList';

function App() {

  return (
    <div className="App">
      <CountryList />
    </div>
  );
}

export default App;
